# DBMovies #

The Application is a Maven project deployed on a WildFly Server. The project use a PostgreSQL database to stock datas with JPA for the persistence. RESTEasy plugin is used to integrate webservices.

## The Structure ##

### Frontend ###

* Angular CLI: 7.3.10
* Node: 14.4.0

### Backend ###

* JavaSE 1.8
* PostgreSQL 11.8
* Maven 3.6.3
* WildFly Server 19.1.0

## Directories and files ##

When you execute the command to clone the project, you get the repository called "dbmovies_1dersc0r" witch contain the source directory "src". 
Inside it you obtain :

* the test folder contain tests files like unit test...
* the main folder which contain the development folders :
    * angular
    * java
    * resources

## Configuration ##

* Run `npm install`
* Run `npm run build` to build the webapp folder into main
* Start your Node server with `npm run start` command
* Start your PostreSQL Server
* Start your WildFly Server
* Execute Maven command `mvn clean install -P deploy-jboss`

In your browser launch localhost:4200