package dbm.data.dto;

import dbm.data.jpa.entity.User;

public class UserDto extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private Integer id;
    private String firstname;
    private String lastname;
    private String email;
    private Integer phone;
    private String adress;
    private Integer zipcode;
    private String city;
    private String login;
    private String password;
	
    
    
    public UserDto() {
	}
	
    public UserDto(Integer id, String firstname, String lastname, String email, Integer phone, String adress, Integer zipcode,
			String city, String login, String password) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.phone = phone;
		this.adress = adress;
		this.zipcode = zipcode;
		this.city = city;
		this.login = login;
		this.password = password;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}
	
	public Integer getPhone() {
		return phone;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public Integer getZipcode() {
		return zipcode;
	}
	public void setZipcode(Integer zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", phone=" + phone + ", adress=" + adress + ", zipcode=" + zipcode + ", city=" + city + ", login="
				+ login + ", password=" + password + "]";
	}
}
