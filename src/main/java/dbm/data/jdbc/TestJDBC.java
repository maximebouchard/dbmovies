package dbm.data.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import dbm.data.jdbc.model.User;

//import dbm.data.model.User;

public class TestJDBC {
	/* La liste qui contiendra tous les résultats de nos essais */
    private List<String> messages = new ArrayList<String>();
    final List<User> result = new ArrayList<>();
    final Logger LOGGER = Logger.getLogger(TestJDBC.class);

    public List<String> executerTests( HttpServletRequest request ) {
        /* Chargement du driver JDBC pour MySQL */
        try {
            messages.add( "Chargement du driver..." );
            Class.forName( "org.postgresql.Driver" );
            messages.add( "Driver chargé !" );
        } catch ( ClassNotFoundException e ) {
            messages.add( "Erreur lors du chargement : le driver n'a pas été trouvé dans le classpath ! <br/>"
                    + e.getMessage() );
        }

        /* Connexion à la base de données */
        String url = "jdbc:postgresql://localhost/dbmovies";
        String utilisateur = "maximebouchard";
        String motDePasse = "";
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            messages.add( "Connexion à la base de données..." );
            connexion = DriverManager.getConnection( url, utilisateur, motDePasse );
            messages.add( "Connexion réussie !" );

            final String SQL_SELECT = "Select * from users";

			PreparedStatement preparedStatement = connexion.prepareStatement(SQL_SELECT);
		
			resultSet = preparedStatement.executeQuery();
            
            /* Création de l'objet gérant les requêtes */
//            statement = connexion.createStatement();
            messages.add( "Objet requête créé !" );

            /* Exécution d'une requête de lecture */
//            resultSet = statement.executeQuery( "SELECT * FROM users;" );
            messages.add( "Requête \"SELECT * FROM users;\" effectuée !" );
            
//            while ( resultSet.next() ) {
//                int idUtilisateur = resultSet.getInt( "id" );
//                String emailUtilisateur = resultSet.getString( "email" );
//                String motDePasseUtilisateur = resultSet.getString( "password" );
//                String nomUtilisateur = resultSet.getString( "firstName" );
//                /* Formatage des données pour affichage dans la JSP finale. */
//                messages.add( "Données retournées par la requête : id = " + idUtilisateur + ", email = " + emailUtilisateur
//                        + ", motdepasse = "
//                        + motDePasseUtilisateur + ", nom = " + nomUtilisateur + "." );
//            }


			final Integer id = resultSet.getInt("id");
			final String firstname = resultSet.getString("firstname");
			final String lastname = resultSet.getString("lastname");
			final String email = resultSet.getString("email");
			final Integer phone = resultSet.getInt("phone");
			final String adress = resultSet.getString("adress");
			final Integer zipcode = resultSet.getInt("zipcode");
			final String city = resultSet.getString("city");
			final String login = resultSet.getString("login");
			final String password = resultSet.getString("password");
			
			messages.add( "Données retournées par la requête : id = " + id + ", email = " + email
                    + ", motdepasse = "
                    + password + ", nom = " + firstname + "." );
			
			final User obj = new User();
			obj.setId(id);
			obj.setFirstname(firstname);
			obj.setLastname(lastname);
			obj.setEmail(email);
			obj.setPhone(phone);
			obj.setAdress(adress);
			obj.setZipcode(zipcode);
			obj.setCity(city);
			obj.setLogin(login);
			obj.setPassword(password);

			result.add(obj);
			
        } catch ( SQLException e ) {
            messages.add( "Erreur lors de la connexion : <br/>"
                    + e.getMessage() );
        } finally {
            messages.add( "Fermeture de l'objet ResultSet." );
            if ( resultSet != null ) {
                try {
                	resultSet.close();
                } catch ( SQLException ignore ) {
                }
            }
            messages.add( "Fermeture de l'objet Statement." );
            if ( statement != null ) {
                try {
                    statement.close();
                } catch ( SQLException ignore ) {
                }
            }
            messages.add( "Fermeture de l'objet Connection." );
            if ( connexion != null ) {
                try {
                    connexion.close();
                } catch ( SQLException ignore ) {
                }
            }
        }
//        result.forEach(x -> LOGGER.info(x.getFirstname()));

        LOGGER.info(messages);
        return messages;
    }
   

}
