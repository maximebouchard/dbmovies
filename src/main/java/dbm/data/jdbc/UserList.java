package dbm.data.jdbc;

import java.sql.*;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import dbm.data.jdbc.model.User;

public class UserList {
    	
    	final Logger LOGGER = Logger.getLogger(UserList.class);
        final List<User> result = new ArrayList<>();

        final String SQL_SELECT = "Select * from users";

        final String url = "jdbc:postgresql://localhost/dbmovies";
        final Properties props = new Properties();
        
        
        public List<User> executerTests (HttpServletRequest request) {

        	props.setProperty("user", "maximebouchard");
            props.setProperty("password", "");
            props.setProperty("ssl", "false");
            
        // auto close connection and preparedStatement
        try (Connection conn = DriverManager.getConnection(url, props);
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            final ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                final Integer id = resultSet.getInt("id");
                final String firstname = resultSet.getString("firstname");
                final String lastname = resultSet.getString("lastname");
                final String email = resultSet.getString("email");
                final Integer phone = resultSet.getInt("phone");
                final String adress = resultSet.getString("adress");
                final Integer zipcode = resultSet.getInt("zipcode");
                final String city = resultSet.getString("city");
                final String login = resultSet.getString("login");
                final String password = resultSet.getString("password");

                final User obj = new User();
                obj.setId(id);
                obj.setFirstname(firstname);
                obj.setLastname(lastname);
                obj.setEmail(email);
                obj.setPhone(phone);
                obj.setAdress(adress);
                obj.setZipcode(zipcode);
                obj.setCity(city);
                obj.setLogin(login);
                obj.setPassword(password);

                result.add(obj);

            }
//            result.forEach(x -> LOGGER.info(x.getFirstname()));

        } catch (final SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (final Exception e) {
            e.printStackTrace();
        }
       
        return result;
        }
}