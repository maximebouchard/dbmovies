package dbm.data.jdbc.model;

public class User {
        private Integer id;
        private String firstname;
        private String lastname;
        private String email;
        private Integer phone;
        private String adress;
        private Integer zipcode;
        private String city;
        private String login;
        private String password;

        public Integer getId() {
            return id;
        }

        public void setId(final Integer id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(final String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(final String lastname) {
            this.lastname = lastname;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(final String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(final String password) {
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(final String email) {
            this.email = email;
        }

        public Integer getPhone() {
            return phone;
        }

        public void setPhone(final Integer phone) {
            this.phone = phone;
        }

        public String getAdress() {
            return adress;
        }

        public void setAdress(final String adress) {
            this.adress = adress;
        }

        public Integer getZipcode() {
            return zipcode;
        }

        public void setZipcode(final Integer zipCode) {
            this.zipcode = zipCode;
        }

        public String getCity() {
            return city;
        }

        public void setCity(final String city) {
            this.city = city;
        }
    }