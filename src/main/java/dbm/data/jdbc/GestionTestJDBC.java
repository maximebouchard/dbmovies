/**
 * this servlet is made for distribute request to jsp file
 */

package dbm.data.jdbc;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbm.data.jdbc.model.User;
import dbm.data.jpa.ObjectsJPA;

/**
 * Servlet implementation class GestionTestJDBC
 */

public class GestionTestJDBC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	public static final String ATT_MESSAGES = "messages";
	public static final String ATT_USERS = "users";
    public static final String VUE = "/test_jdbc.jsp";
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionTestJDBC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /* Initialisation de l'objet Java et récupération des messages */
        TestJDBC test = new TestJDBC();
        UserList userList = new UserList();
        ObjectsJPA consoleJPA = new ObjectsJPA();
        
        List<String> messages = test.executerTests( request );
        List<User> users = userList.executerTests(request);
        

        /* Enregistrement de la liste des messages dans l'objet requête */
        request.setAttribute( ATT_MESSAGES, messages );
        request.setAttribute( ATT_USERS, users );
        request.setAttribute("consoleJPA", consoleJPA.executerTests(request));

        /* Transmission vers la page en charge de l'affichage des résultats */
        this.getServletContext().getRequestDispatcher(VUE).forward( request, response );
//        this.getServletContext().getAttribute(ATT_MESSAGES);
//        this.getServletContext().getAttribute(ATT_USERS);
        
    }

}
