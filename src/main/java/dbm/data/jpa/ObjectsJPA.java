package dbm.data.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import dbm.data.jpa.entity.Movie;
import dbm.data.jpa.entity.User;

public class ObjectsJPA {
	final Logger LOGGER = Logger.getLogger(ObjectsJPA.class);

//	public static void main(String[] args) {
	public List<User> executerTests(HttpServletRequest request) {
		// TODO Auto-generated method stub

		EntityManagerFactory entityManagerFactory = null;
		EntityManager entityManager = null;

		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("dbmovies_1dersc0r-persistence-unit");
			entityManager = entityManagerFactory.createEntityManager();

			System.out.println("- Lecture de tous les utilisateurs -----------");

			List<User> users = entityManager.createQuery("from User", User.class).getResultList();
			for (User user : users) {
				LOGGER.info(user.getFirstname());
//				System.out.println(user.getFirstname());
			}

			return users;

		} finally {
			if (entityManager != null)
				entityManager.close();
			if (entityManagerFactory != null)
				entityManagerFactory.close();
		}
	}
//	}

	public List<User> returnUsers() {

		EntityManagerFactory entityManagerFactory = null;
		EntityManager entityManager = null;
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("dbmovies_1dersc0r-persistence-unit");
			entityManager = entityManagerFactory.createEntityManager();

			System.out.println("- Lecture de tous les utilisateurs -----------");

			List<User> users = entityManager.createQuery("from User", User.class).getResultList();
			for (User user : users) {
				System.out.println(user.getFirstname());
				LOGGER.info(user.getFirstname());
			}

			return users;

		} finally {
			if (entityManager != null)
				entityManager.close();
			if (entityManagerFactory != null)
				entityManagerFactory.close();
		}
	}

//	public User createUser(User user) {
//		return user;
//		
//	}

	public void createUser(User newUser) {
		EntityManagerFactory entityManagerFactory = null;
		EntityManager entityManager = null;

		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("dbmovies_1dersc0r-persistence-unit");
			entityManager = entityManagerFactory.createEntityManager();

			LOGGER.info("- Insertion d'un utilisateur -----------");

			EntityTransaction trans = entityManager.getTransaction();
			trans.begin();

//		User newUser = new User("Joe", "Dan", "j.dan@email.com", "22 rue de la rue", 11111, "Ville", "joe", "dan");
//		User newUser = new User(user.get"Joe", "Dan", "j.dan@email.com", "22 rue de la rue", 11111, "Ville", "joe", "dan");
			entityManager.persist(newUser);

			List<User> results = entityManager.createQuery("from User", User.class).getResultList();
			for (User user : results) {
				System.out.println(user);
				LOGGER.debug(user);
			}

			trans.commit();

//		return results;

		} finally {
			if (entityManager != null)
				entityManager.close();
			if (entityManagerFactory != null)
				entityManagerFactory.close();
		}
	}

	public List<Movie> returnMovies() {

		EntityManagerFactory entityManagerFactory = null;
		EntityManager entityManager = null;

		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("dbmovies_1dersc0r-persistence-unit");
			entityManager = entityManagerFactory.createEntityManager();

			List<Movie> movies = entityManager.createQuery("from Movie", Movie.class).getResultList();

			return movies;

		} finally {
			if (entityManager != null)
				entityManager.close();
			if (entityManagerFactory != null)
				entityManagerFactory.close();
		}
	}

	public void deleteUser(int id) {
		EntityManagerFactory entityManagerFactory = null;
		EntityManager entityManager = null;

		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("dbmovies_1dersc0r-persistence-unit");
			entityManager = entityManagerFactory.createEntityManager();

			User user = entityManager.find(User.class, id);
			
			LOGGER.info("- Suppression de l'utilisateur " + user.getFirstname() + " " + user.getLastname() + "-----------");

			EntityTransaction trans = entityManager.getTransaction();
			trans.begin();

//			Query query = entityManager.createQuery("DELETE FROM users u WHERE u.id = " + id);
//			int deletedCount = query.executeUpdate();

			entityManager.remove(user);
			
//			User newUser = new User("Joe", "Dan", "j.dan@email.com", "22 rue de la rue", 11111, "Ville", "joe", "dan");
//			User newUser = new User(user.get"Joe", "Dan", "j.dan@email.com", "22 rue de la rue", 11111, "Ville", "joe", "dan");
//			entityManager.remove(entity);

//			entityManager.createQuery("delete from User where id = " + id + "").executeUpdate();

//			List<User> results = entityManager.createQuery("from User", User.class).getResultList();
//		    for( User user : results) {
//	            System.out.println(user);
//	            LOGGER.debug(user);
//	        }
//	        
			trans.commit();
//			
//			return results;

		} finally {
			if (entityManager != null)
				entityManager.close();
			if (entityManagerFactory != null)
				entityManagerFactory.close();
		}
	}
}
