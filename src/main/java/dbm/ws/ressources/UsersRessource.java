package dbm.ws.ressources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import dbm.data.jpa.ObjectsJPA;
import dbm.data.jpa.entity.User;

@Path("/users")
public class UsersRessource {

	final Logger LOGGER = Logger.getLogger(UsersRessource.class);
	ObjectsJPA objectsJPA = new ObjectsJPA();

	@GET
	@Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> findAll() {
		return objectsJPA.returnUsers();
	}

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void createUser(User user) {

		LOGGER.info(user);

		objectsJPA.createUser(user);
	}

	@DELETE
	@Path("/delete/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteUser(@PathParam("id") int id) {
		LOGGER.info("Suppression de l'utilisateur id " + id);

		objectsJPA.deleteUser(id);
	}

}
