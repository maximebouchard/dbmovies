package dbm.ws.ressources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dbm.data.jpa.ObjectsJPA;
import dbm.data.jpa.entity.Movie;

@Path("/movies")
public class MoviesRessource {

	ObjectsJPA objectsJPA = new ObjectsJPA();

	@GET
	@Path("/findAll")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Movie> findAll() {
		return objectsJPA.returnMovies();
	}
}