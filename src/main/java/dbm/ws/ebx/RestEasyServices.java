package dbm.ws.ebx;

import javax.ws.rs.core.Application;

import dbm.ws.ressources.MoviesRessource;
import dbm.ws.ressources.UsersRessource;

import javax.ws.rs.ApplicationPath;
import java.util.HashSet;
import java.util.Set;

/**
* The JAX-RS application configuration class for these Rest Web services * @version 1.0
*/

@ApplicationPath("/api")
public class RestEasyServices extends Application{

    private Set<Class<?>> classes = new HashSet();

    public RestEasyServices() {
        classes.add(MoviesRessource.class);
        classes.add(UsersRessource.class);
    }


    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

}