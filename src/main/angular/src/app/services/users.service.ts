import { Injectable } from '@angular/core';
import { USERS } from '../users/mock-user';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import * as _ from 'lodash';
import { User } from '../users/user';
import { Movie } from '../movies/movie';
import { Observable } from 'rxjs/internal/Observable';
import { RESOURCE_CACHE_PROVIDER } from '@angular/platform-browser-dynamic';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  usersList: User[] = [];

  constructor(private http: HttpClient) { }

  searchUser(login: string, password: string): User {
    return _.find(USERS, { login, password });
  }

  checkIfRegister(login: string, password: string): boolean {
    if (!_.find(USERS, { login, password })) {
      return false;
    } else {
      return true;
    }
  }

  getMoviesList(user: User): Movie[] {
    return user.moviesList;
  }

  getAll(): Observable<any> {
    return this.http.get('/api/users/findAll');	
  }

  createNewUser(user: User) {
    // console.log(user);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    }
      
    return this.http.post<any>('/api/users/create', 
    {
      firstname: user.firstname, 
      lastname: user.lastname, 
      email: user.email, 
      adress: user.adress,
      zipcode: user.zipcode, 
      city: user.city,
      login: user.login,
      password: user.password
    },
    httpOptions).subscribe(value => {
      console.log(value);
    })
  }

  delete(id: number) {
    return this.http.delete('/api/users/delete/'+ id).subscribe(value => {
      // console.log(value);
    });
  }
}
