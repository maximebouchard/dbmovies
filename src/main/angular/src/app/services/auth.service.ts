import { Injectable } from '@angular/core';
import { User } from '../users/user';
import { USERS } from '../users/mock-user';

import * as _ from 'lodash';
import { Movie } from '../movies/movie';
import { MOVIES } from '../movies/mock-movies';
import { JsonPipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isAuth: boolean = false;
  user: User;

  signIn(currentUser: User) {
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            this.isAuth = true;
            resolve(true);
          }, 1000
        );
      }
    );
  }

  signOut() {
    sessionStorage.clear();
    console.log("Session stop", sessionStorage);
    this.isAuth = false;
  }

  getUserById(id: number): User {
    this.user = _.find(USERS, { 'id': id });
    console.log("Utilisateur connecté : " + sessionStorage.getItem('firstname'));
    return this.user;
  }

  setSession(user: User): void {
    sessionStorage.setItem('id', String(user.id));
    sessionStorage.setItem('firstname', user.firstname);
    sessionStorage.setItem('lastname', user.lastname);
    sessionStorage.setItem('email', user.email);
    sessionStorage.setItem('phone', user.phone);
    sessionStorage.setItem('adress', user.adress);
    sessionStorage.setItem('zipcode', user.zipcode);
    sessionStorage.setItem('city', user.city);
    sessionStorage.setItem('login', user.login);
    sessionStorage.setItem('password', user.password);
    sessionStorage.setItem('subdate', user.subdate);
    sessionStorage.setItem('moviesList', JSON.stringify(user.moviesList))

    console.log("Session start: ", sessionStorage);
  }
}
