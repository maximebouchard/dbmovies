import { Injectable } from '@angular/core';
import { DialogElementComponent } from '../dialog-element/dialog-element.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) { }

  open(): void {
    this.dialog.open(DialogElementComponent, {
      data: { 
        title: "Confirmation de supression",
        content: 'Êtes-vous sûr de vouloir supprimer votre compte. Cette action est irréversible.',
       },
    });
  }

}