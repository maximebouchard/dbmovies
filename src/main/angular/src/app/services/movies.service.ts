import { Injectable } from '@angular/core';
import { Movie } from '../movies/movie';
import { MOVIES } from '../movies/mock-movies';

import * as _ from 'lodash';
import { User } from '../users/user';
import { USERS } from '../users/mock-user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  user: User;
  movie: Movie;
  moviesList: Movie[] = [];

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get('/api/movies/findAll');	
  }

  getMovieById(id: number): Movie {
    this.getAll().subscribe(value => {
      value.forEach(element => {
        this.moviesList.push(element);
      });
    });
    console.log(this.moviesList);
    
    return _.find(this.moviesList, ['id', Number(id)]);
    // return _.find(MOVIES, ['id', Number(id)]);
  }

  rentMovieByUser(movieId: number, userId: string): void {
    this.user = _.find(USERS, ['id', Number(sessionStorage.getItem('id'))]);
    this.movie = _.find(MOVIES, ['id', movieId]);
    console.log(this.user);
    console.log(this.movie);
    this.user.moviesList.push(this.movie);
    console.log(this.user.moviesList);
  }

}
