import { Hero } from './hero';

export const HEROES: Hero[] = [{
    id: 1,
    name: "Superman",
    streight: 200,
    capacity: "fly"
  },
  {
    id: 2,
    name: "Magneto",
    streight: 100,
    capacity: "magnetic"
  },
  {
    id: 3,
    name: "Intenso",
    streight: 120,
    capacity: "fast"
  },
  {
    id: 4,
    name: "Ragnarock",
    streight: 500,
    capacity: "strong"
  },
  {
    id: 5,
    name: "Tifonus",
    streight: 50,
    capacity: "agile"
  }]