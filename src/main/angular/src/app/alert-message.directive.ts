import { Directive, OnInit } from '@angular/core';

@Directive({
  selector: '[appAlertMessage]'
})
export class AlertMessageDirective implements OnInit {

  constructor() { }

  ngOnInit()    {
     this.logIt(`onInit`); }


  private logIt(msg: string) {
    console.log(msg);
    return "toto";
  }

}
