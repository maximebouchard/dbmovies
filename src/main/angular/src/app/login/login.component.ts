import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { User } from '../users/user';
import { AuthService } from '../services/auth.service';
import { Movie } from '../movies/movie';
import { USERS } from '../users/mock-user';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { UsersService } from '../services/users.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  currentUser: User;
  storage: boolean;
  users: User[] = USERS;
  usersList: User[] = [];
  alert: string = '';
  authStatus: boolean = this.authService.isAuth;
  randomLogin: string;
  sessionStorage: Storage;
  moviesList: Movie[];
  
  loginForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl(''),
  });

  @Output() public sessionName = new EventEmitter<string>();

  constructor(private usersService: UsersService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.usersService.getAll().subscribe(value => {
      value.forEach(element => {
        this.usersList.push(element);
      });
    })

    console.log("list of users", this.usersList);

    // this.fadeInOutJS();

    this.authStatus = this.authService.isAuth;
    // this.randomLogin = _.sample(USERS).login;
    this.sessionStorage = sessionStorage;
  }
  
  eventSession(): void {
    if (sessionStorage.getItem('firstname')) {
      this.sessionName.emit(sessionStorage.getItem('firstname') + " " + sessionStorage.getItem('lastname'));
    } else {
      this.sessionName.emit('');
    }
  }

  onSubmit(): void {
    var login: string = this.loginForm.controls.login.value;
    var password: string = this.loginForm.controls.password.value;

    this.currentUser = _.find(this.usersList, { login, password });

    if (this.currentUser) {
      this.onSignIn();
      this.alert = 'Bienvenue ' + this.currentUser.firstname;
      this.moviesList = this.currentUser.moviesList;
    } else {
      this.alert = 'Identifiant ou mot de passe incorrect';
    }

    // this.fadeInOutJS();
  }

  onSignIn(): void {
    this.authService.signIn(this.currentUser).then(
      () => {
        this.authService.setSession(this.currentUser);
        console.log('Sign in successful!');
        // On envoie au component parent
        this.eventSession();
        this.router.navigate(['user'], { queryParams: { id: this.currentUser.id }})
        // this.router.navigate(['movies']);
      }
    );
  }

}
