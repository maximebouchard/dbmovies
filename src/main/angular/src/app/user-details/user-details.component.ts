import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { User } from '../users/user';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../services/users.service';
import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs/internal/Subscription';
import * as _ from 'lodash';
import * as $ from 'jquery';
import { USERS } from '../users/mock-user';
import { Movie } from '../movies/movie';
import { FormGroup, FormControl } from '@angular/forms';
import { DialogService } from '../services/dialog.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit, OnDestroy {

  @Output() public sessionName = new EventEmitter<string>();

  constructor(private dialogService: DialogService, private router: Router, private authService: AuthService, private usersService: UsersService, private route: ActivatedRoute) { }

  currentUser: User;
  isAuth: boolean = this.authService.isAuth;
  subscription: Subscription = new Subscription();
  sessionStorage: Storage;
  list: User[] = [];

  storage: boolean;
  users: User[] = USERS;
  usersList: User[] = [];
  alert: string = '';
  authStatus: boolean = this.authService.isAuth;
  randomLogin: string;
  moviesList: Movie[];

  loginForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl(''),
  });

  ngOnInit(): void {

    this.route.queryParams.subscribe({
      next: value => {
        // this.id = Number(value.id);
        // console.log(value.id);
        this.subscription.add(this.usersService.getAll().subscribe(val => {
          val.forEach(element => {
            this.list.push(element);
          });
          this.currentUser = _.find(this.list, { id: Number(value.id) });
        }));

      },
      error: err => console.error(err),
      complete: () => console.log('DONE!')
    });

    console.log(this.authStatus);

    //TODO: checker si connecté
    console.log(this.currentUser);

    // if (!this.isAuth) {
    //   this.router.navigate(['']);
    // }

    this.authStatus = this.authService.isAuth;
    this.randomLogin = _.sample(USERS).login;
    this.sessionStorage = sessionStorage;
  }

  eventSession(): void {
    if (sessionStorage.getItem('firstname')) {
      this.sessionName.emit(sessionStorage.getItem('firstname') + " " + sessionStorage.getItem('lastname'));
    } else {
      this.sessionName.emit('');
    }
  }

  onSignOut(): void {
    // this.loginForm.reset();
    this.authService.signOut();
    this.eventSession();
    this.authStatus = this.authService.isAuth;
    this.alert = "Au revoir " + this.currentUser.firstname;
    this.fadeInOutJS();
    this.currentUser = null;
    this.router.navigate(['']);
  }

  onDeleteUser(id: number): void {
    this.usersService.delete(id);
    _.pull(this.usersList, this.currentUser);
    this.onSignOut();
  }

  openDialog() {
    let data: {
      title: "Confirmation de supression",
      content: 'Êtes-vous sûr de vouloir supprimer votre compte. Cette action est irréversible.',
    }
    this.dialogService.open();
  }

  fadeInOutJS(): void {
    $(document).ready(function () {
      $('.btnSubmitLogin').click(function () {
        $('.alert').fadeIn('slow');
      })
    });

    $(document).ready(function () {
      window.setTimeout(function () {
        $('.alert').fadeOut('slow');
      }, 1500)
    })
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
