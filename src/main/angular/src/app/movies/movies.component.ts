import { Component, OnInit } from '@angular/core';
import { Movie } from './movie';
import { MOVIES } from './mock-movies';
import { MoviesService } from '../services/movies.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  selectedMovie: Movie;
  movies: Movie[] = MOVIES;
  moviesList: Movie[] = []; 

  constructor(private router: Router, private moviesService: MoviesService) { }

  ngOnInit(): void {

    this.moviesService.getAll().subscribe(value => {
      console.log(value);
      value.forEach(element => {
        this.moviesList.push(element);
      });
    });
  }

  onSelect(id: number): void{
    this.router.navigate(['movie'], {queryParams: { id: id }});
  }

}
