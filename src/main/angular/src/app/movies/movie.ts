export interface Movie {
    id: number;
    title: string;
    img: string;
    synopsis: string;
    note: number;
    price: number;
}
