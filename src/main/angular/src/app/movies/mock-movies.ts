import { Movie } from './movie';

export const MOVIES: Movie[] = [
    {
        id: 1,
        title: "casino",
        img: "jpeg",
        synopsis: "lorem ipsum dolor sit amet",
        note: 10,
        price: 4.99
    },
    {
        id: 2,
        title: "scarface",
        img: "jpeg",
        synopsis: "lorem ipsum dolor sit amet",
        note: 9,
        price: 4.99
    },
    {
        id: 3,
        title: "et au milieu coule une rivière",
        img: "jpeg",
        synopsis: "lorem ipsum dolor sit amet",
        note: 10,
        price: 4.99
    }
]