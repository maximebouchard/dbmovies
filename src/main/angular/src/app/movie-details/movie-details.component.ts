import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../movies/movie';
import { MoviesService } from '../services/movies.service';
import { UsersService } from '../services/users.service';
import { Subscription } from 'rxjs/internal/Subscription';
import * as _ from 'lodash';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit, OnDestroy{

  constructor(private route: ActivatedRoute, private router: Router, private moviesService: MoviesService, private usersService: UsersService) { }

  currentMovie: Movie;
  subscription: Subscription = new Subscription();
  id: number;
  list: Movie[] = [];


  ngOnInit(): void {
    this.route.queryParams.subscribe({
      next: value => {
        this.id = Number(value.id);
        // console.log(value.id);
        this.subscription.add(this.moviesService.getAll().subscribe(val => {
          val.forEach(element => {
            this.list.push(element);
          });
          this.currentMovie = _.find(this.list, {id : this.id });
        }));
    
      },
      error: err => console.error(err),
      complete: () => console.log('DONE!')
    });
  }
  
  onRentIt(movie: Movie): void {
    if (sessionStorage.length === 0) {
      this.router.navigate(['/subscribe'], {queryParams: { 'rate' : movie.id }});
    } else {
      // TODO: call a webservice to rate the movie
      this.moviesService.rentMovieByUser(movie.id, sessionStorage.getItem('id'))
      // console.log(movie);
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
