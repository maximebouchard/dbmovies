import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../users/user';
import { UsersComponent } from '../users/users.component';
import { USERS } from '../users/mock-user';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-logon',
  templateUrl: './logon.component.html',
  styleUrls: ['./logon.component.css']
})
export class LogonComponent implements OnInit {

  newUser: User;

  subscribeForm = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    email: new FormControl(''),
    adress: new FormControl(''),
    zipcode: new FormControl(''),
    city: new FormControl(''),
    login: new FormControl(''),
    password: new FormControl(''),
  });
  
  constructor(private usersService: UsersService) { }
  
  ngOnInit() {
  }

  onSubmit(): void {
    this.newUser = {
        id: null,
        firstname: this.subscribeForm.controls['firstname'].value,
        lastname: this.subscribeForm.controls['lastname'].value,
        email: this.subscribeForm.controls['email'].value,
        phone: null,
        adress: this.subscribeForm.controls['adress'].value,
        zipcode: this.subscribeForm.controls['zipcode'].value,
        city: this.subscribeForm.controls['city'].value,
        login: this.subscribeForm.controls['login'].value,
        password: this.subscribeForm.controls['password'].value,
        subdate: String(new Date('yyyy')),
        moviesList: [
        ]
    };
    // USERS.push(this.newUser);
    // console.log(USERS);

    this.usersService.createNewUser(this.newUser);
    
  }

}
