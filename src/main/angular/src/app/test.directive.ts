import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appTest]'
})
export class TestDirective {

  constructor(element: ElementRef, renderer: Renderer2) { 
    // console.log(element.nativeElement);
    renderer.setStyle(element.nativeElement, 'color', 'red');
  }

}
