import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { MoviesComponent } from './movies/movies.component';
import { UsersComponent } from './users/users.component';
import { HomeComponent } from './home/home.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { LogonComponent } from './logon/logon.component';
import { DialogElementComponent } from './dialog-element/dialog-element.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { LoginComponent } from './login/login.component';
import { AlertMessageDirective } from './alert-message.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    MoviesComponent,
    UsersComponent,
    HomeComponent,
    MovieDetailsComponent,
    LogonComponent,
    DialogElementComponent,
    UserDetailsComponent,
    LoginComponent,
    AlertMessageDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatIconModule,
    MatDialogModule,
    BrowserAnimationsModule
  ],
  entryComponents: [
    DialogElementComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
