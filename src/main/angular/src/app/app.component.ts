import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(private authService: AuthService) {}

  title: string = 'DBmovies';
  btnLogin: string = sessionStorage.getItem('firstname') + " " + sessionStorage.getItem('lastname');
  subscription : Subscription = new Subscription();

  ngOnInit():void {
    this.btnLogin = "";
  }

  onActivate(componentReference) {
    console.log("reference : ", componentReference);
    if(componentReference.sessionName){
    this.subscription = componentReference.sessionName.subscribe((data) => {
      this.btnLogin = data;
   })
  }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
