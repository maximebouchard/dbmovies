import {User} from './user';
import { MOVIES } from '../movies/mock-movies';

export const USERS: User[] = [
  {
    id: 1,
    firstname: 'Maxime',
    lastname: 'Bouchard',
    email: 'maxime.bchrd@gmail.com',
    phone: '0603770023',
    adress: '224 avenue André Maginot',
    zipcode: '37100',
    city: 'Tours',
    login: 'admin',
    password: 'admin',
    subdate: '2007',
    moviesList: [
      MOVIES[0],
      MOVIES[2]
    ]
  },
  {
    id: 2,
    firstname: 'Jean',
    lastname: 'Dupont',
    email: 'jean.dupont@gmail.com',
    phone: '0632783445',
    adress: '12 allée des Lilas',
    zipcode: '68200',
    city: 'Mulhouse',
    login: 'test',
    password: '123',
    subdate: '1999',
    moviesList: []
  },
  {
    id: 3,
    firstname: 'Fiona',
    lastname: 'Lafitte',
    email: 'fiona.laffitte@free.fr',
    phone: '0327896354',
    adress: 'rue de la Plage',
    zipcode: '06150',
    city: 'Cannes',
    login: 'max',
    password: 'toto123',
    subdate: '2003',
    moviesList: []
  }
];
