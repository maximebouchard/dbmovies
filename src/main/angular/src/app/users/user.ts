import { Movie } from '../movies/movie';
import { MOVIES } from '../movies/mock-movies';

export interface User {
    id: number,
    firstname: string,
    lastname: string,
    email: string,
    phone: string,
    adress: string,
    zipcode: string,
    city: string,
    subdate: string,
    login: string;
    password: string;
    moviesList: Movie[];
}
