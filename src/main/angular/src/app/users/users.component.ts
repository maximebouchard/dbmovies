import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { User } from './user';
import { Movie } from '../movies/movie';
import { USERS } from './mock-user';
import * as $ from 'jquery';

import * as _ from 'lodash';
import { Router, RouterLink } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MoviesService } from '../services/movies.service';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {

  @Output() public sessionName = new EventEmitter<string>();

  constructor(private router: Router, private authService: AuthService, private movieService: MoviesService, private usersService: UsersService) { }

  storage: boolean;
  users: User[] = USERS;
  currentUser: User;
  usersList: User[] = [];
  alert = '';
  authStatus: boolean = this.authService.isAuth;
  randomLogin: string;
  sessionStorage: Storage;
  moviesList: Movie[];

  loginForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl(''),
  });

  ngOnInit(): void {

    this.usersService.getAll().subscribe(value => {
      value.forEach(element => {
        this.usersList.push(element);
      });
    })

    console.log("list of users", this.usersList);


    this.authStatus = this.authService.isAuth;
    this.randomLogin = _.sample(USERS).login;
    this.sessionStorage = sessionStorage;
  }

  eventSession() {
    if (sessionStorage.getItem('firstname')) {
      this.sessionName.emit(sessionStorage.getItem('firstname') + " " + sessionStorage.getItem('lastname'));
    } else {
      this.sessionName.emit('');
    }
  }

  onSubmit(): void {
    // this.user = this.usersService.searchUser(this.loginForm.controls.login.value, this.loginForm.controls.password.value);
    var login: string = this.loginForm.controls.login.value;
    var password: string = this.loginForm.controls.password.value;

    this.currentUser = _.find(this.usersList, { login, password });

    if (this.currentUser) {
      this.onSignIn();
      this.alert = 'Bienvenue ' + this.currentUser.firstname;
      this.moviesList = this.currentUser.moviesList;
    } else {
      this.alert = 'Identifiant ou mot de passe incorrect';
    }
  }

  onSignIn(): void {
    this.authService.signIn(this.currentUser).then(
      () => {
        this.authService.setSession(this.currentUser);
        console.log('Sign in successful!');
        // On envoie au component parent
        this.eventSession();
      }
    );
  }

  onSignOut(): void {
    this.loginForm.reset();
    this.authService.signOut();
    this.eventSession();
    this.authStatus = this.authService.isAuth;
    this.alert = "Au revoir " + this.currentUser.firstname;
    this.fadeOutJS();
    this.currentUser = null;
  }

  onGoSubscribe(): void {
    this.router.navigate(['subscribe']);
  }

  onDeleteUser(id: number): void {
    this.usersService.delete(id);
    this.onSignOut();
    _.pull(this.usersList, this.currentUser);
  }

  fadeOutJS() {
    $(document).ready(function () {
      window.setTimeout(function () {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function () {
          $(this).remove();
        });
      }, 1500);
    });
  }
}
