import { Injectable } from '@angular/core';
import {Router, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService} from './services/auth.service';
import * as _ from 'lodash';
import { UsersService } from './services/users.service';
import { User } from './users/user';

@Injectable({
  providedIn: 'root'
})
export class AuthentGuard implements CanActivate {

  usersList: User[] = [];
  
  constructor(private router: Router, private authService: AuthService, private usersService: UsersService){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // if (this.authService.isAuth) {
    
    // let registerd: boolean = this.usersService.searchUser(sessionStorage.getItem('login'));
    // this.usersService.searchUser;
    // this.usersService.getAll().subscribe(value => {
    //   value.forEach(element => {
    //     this.usersList.push(element);
    //   });
    // })
    // return _.find(this.usersList, ['id',  Number(sessionStorage.getItem('id'))]);

    if (sessionStorage.getItem('login')) {
      return true;
    } else {
      this.router.navigate(['']);
    }
  }
}
