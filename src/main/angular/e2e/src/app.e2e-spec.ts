import { AppPage } from './app.po';
import { User } from '../../src/app/users/user';
import { USERS } from '../../src/app/users/mock-user';
import * as _ from 'lodash';

import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  const users: User[] = USERS;

  let page: AppPage;
  const randomLogin: string = this.randomLogin = _.sample(USERS).login;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Welcome to DBmovies!');
  });

  it('should display navBar and footer', () => {
    page.navigateTo();
    // browser.pause();
    expect(page.getNav());
    expect(page.getFooter());
  });

  it('should display My Account button', () => {
    page.navigateTo();
    expect(page.getMyAccountButton().getText()).toEqual('My account');
  });

  it('should route to My Account page', () => {
    page.navigateTo();
    page.getMyAccountButton().click();
    expect(page.getMyAccountTitle().getText()).toEqual('User account');
  });

  it('should be log in', () => {
    page.navigateToLogin();
    // page.setLogin().sendKeys('admin');
    // page.setPassword().sendKeys('admin');
    page.setLogin().sendKeys(randomLogin);
    page.setPassword().sendKeys(_.find(USERS, ['login', this.randomLogin]).password);
    page.clickSubmit();
    expect(page.getHomeTitle().getText()).toEqual('Home');
  });

  it('should be rejected', () => {
    page.navigateToLogin();
    page.setLogin().sendKeys('admin');
    page.setPassword().sendKeys('adin');
    page.clickSubmit();
    expect(page.getAlert().getText()).toEqual('Identifiant ou mot de passe incorrect');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
