import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  navigateToLogin() {
    return browser.get(browser.baseUrl + '/users') as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText() as Promise<string>;
  }

  setLogin() {
    return element(by.css('[formcontrolname="login"]'));
  }

  clickSubmit() {
    return element(by.css('[type="submit"]')).click();
  }

  setPassword() {
    return element(by.css('[formcontrolname="password"]'));
  }

  getAlert() {
    return element(by.tagName('app-root p'));
  }

  getHomeTitle() {
    return element(by.css('app-home h1'));
  }

  getMyAccountTitle() {
    return element(by.tagName('app-users h1'));
  }

  getMyAccountButton() {
    return element(by.css('[routerlink="/users"]'));
  }

  getNav() {
    return element(by.tagName('nav')).getTagName();
  }

  get() {
    return element(by.css('app-root nav')).getTagName() as Promise<string>;
  }

  getFooter() {
    return element(by.css('app-root footer')).getTagName() as Promise<string>;
  }
}
