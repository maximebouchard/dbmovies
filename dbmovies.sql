--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 12.2

-- Started on 2020-08-08 12:24:51 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 198 (class 1259 OID 16442)
-- Name: movies; Type: TABLE; Schema: public; Owner: maximebouchard
--

CREATE TABLE public.movies (
    id integer NOT NULL,
    title character varying(50) NOT NULL,
    img character varying(100),
    synopsis text NOT NULL,
    note smallint,
    price real NOT NULL
);


ALTER TABLE public.movies OWNER TO maximebouchard;

--
-- TOC entry 3230 (class 0 OID 0)
-- Dependencies: 198
-- Name: TABLE movies; Type: COMMENT; Schema: public; Owner: maximebouchard
--

COMMENT ON TABLE public.movies IS 'Table des films';


--
-- TOC entry 3231 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN movies.title; Type: COMMENT; Schema: public; Owner: maximebouchard
--

COMMENT ON COLUMN public.movies.title IS 'Titre du film';


--
-- TOC entry 3232 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN movies.synopsis; Type: COMMENT; Schema: public; Owner: maximebouchard
--

COMMENT ON COLUMN public.movies.synopsis IS 'Descriptif du film';


--
-- TOC entry 3233 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN movies.note; Type: COMMENT; Schema: public; Owner: maximebouchard
--

COMMENT ON COLUMN public.movies.note IS 'Note du film';


--
-- TOC entry 3234 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN movies.price; Type: COMMENT; Schema: public; Owner: maximebouchard
--

COMMENT ON COLUMN public.movies.price IS 'Prix de location';


--
-- TOC entry 199 (class 1259 OID 16445)
-- Name: movies_id_seq; Type: SEQUENCE; Schema: public; Owner: maximebouchard
--

CREATE SEQUENCE public.movies_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movies_id_seq OWNER TO maximebouchard;

--
-- TOC entry 3235 (class 0 OID 0)
-- Dependencies: 199
-- Name: movies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: maximebouchard
--

ALTER SEQUENCE public.movies_id_seq OWNED BY public.movies.id;


--
-- TOC entry 197 (class 1259 OID 16422)
-- Name: users; Type: TABLE; Schema: public; Owner: maximebouchard
--

CREATE TABLE public.users (
    id integer NOT NULL,
    firstname character varying(50) NOT NULL,
    lastname character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    phone integer,
    adress character varying(100) NOT NULL,
    zipcode integer NOT NULL,
    city character varying(50) NOT NULL,
    login character varying(50) NOT NULL,
    password character varying(50) NOT NULL
);


ALTER TABLE public.users OWNER TO maximebouchard;

--
-- TOC entry 196 (class 1259 OID 16420)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: maximebouchard
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO maximebouchard;

--
-- TOC entry 3236 (class 0 OID 0)
-- Dependencies: 196
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: maximebouchard
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 3095 (class 2604 OID 16447)
-- Name: movies id; Type: DEFAULT; Schema: public; Owner: maximebouchard
--

ALTER TABLE ONLY public.movies ALTER COLUMN id SET DEFAULT nextval('public.movies_id_seq'::regclass);


--
-- TOC entry 3094 (class 2604 OID 16425)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: maximebouchard
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3223 (class 0 OID 16442)
-- Dependencies: 198
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: maximebouchard
--

COPY public.movies (id, title, img, synopsis, note, price) FROM stdin;
1	Mission Impossible	movie_1.jpg	Un bon film avec Tom Cruise	8	4.98999977
2	Bad Boys	movie_2.jpg	Un bon film avec Will Smith	10	6.98999977
\.


--
-- TOC entry 3222 (class 0 OID 16422)
-- Dependencies: 197
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: maximebouchard
--

COPY public.users (id, firstname, lastname, email, phone, adress, zipcode, city, login, password) FROM stdin;
88	Veronica	Bouchard	maxime.bchrd@gmail.com	\N	A	12	A	x	x
89	Veronica	Bouchard	maxime.bchrd@gmail.com	\N	A	12	A	x	x
90	Maxime	Bouchard	maxime.bchrd@gmail.com	\N	A	12	A	a	a
\.


--
-- TOC entry 3237 (class 0 OID 0)
-- Dependencies: 199
-- Name: movies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: maximebouchard
--

SELECT pg_catalog.setval('public.movies_id_seq', 2, true);


--
-- TOC entry 3238 (class 0 OID 0)
-- Dependencies: 196
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: maximebouchard
--

SELECT pg_catalog.setval('public.users_id_seq', 92, true);


--
-- TOC entry 3099 (class 2606 OID 16453)
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: public; Owner: maximebouchard
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id);


--
-- TOC entry 3097 (class 2606 OID 16437)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: maximebouchard
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


-- Completed on 2020-08-08 12:24:51 CEST

--
-- PostgreSQL database dump complete
--

